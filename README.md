# Arch Linux x LUKS x DRACUT

`dracut --uefi --include /etc/dracuf.conf.d/rd.live.overlay/ / /efi/EFI/arch/linux.ef`

Creates a unified kernel image including kernel, microcode, initramfs and kernel command line at /efi/EFI/arch/linux.efi. Executable should be auto-detected by boot managers such as refind or systemd-boot.

You can also omit --uefi and generate a traditional initramfs-linux.img. It will still include microcode and a command line.