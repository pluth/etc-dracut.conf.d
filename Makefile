help:

.PHONY: source.tar
source.tar: usr/**/*
	tar -c -f $@ $^
	updpkgsums

install_config: etc/
	mkdir -p /etc/dracut.conf.d/
	mkdir -p /etc/cmdline.d/
	ln -f etc/cmdline.d/* /etc/cmdline.d/
	ln -f etc/dracut.conf.d/* /etc/dracut.conf.d/

package: PKGBUILD source.tar
	makepkg -sf --sign

